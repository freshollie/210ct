#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;
char board[9];
char turn = 'X';
char y = 'O';

bool checker(int start, int stop, int step)
{
    for(start; start<stop; start+=step){
        if(board[start] != turn)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    fill(board,board+9,' ');
    char message=' ';
    while(find(board, board+9, ' ')!=board+9 && message == ' ')
    {
        int index;
        cout<<turn;
        cin >> index;
        if(board[index] == ' ')
        {
            board[index] = turn;
        }
        string boardString(board);
        for(int i = 0; i<9; i+=3)
        {
            cout<<boardString.substr(i, 3)<<endl;
            if (checker(i/3,9,3)|
                checker(i,i+3,1)|
                checker(0,9,4)|
                checker(2,8,2))
                {
                    message = turn;
                }
        }

        swap(y, turn);
    }
    cout<<message;
}
