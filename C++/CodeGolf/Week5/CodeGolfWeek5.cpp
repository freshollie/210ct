#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

bool solve(char board[81], int index=0)
{
    int j, column, row, startIndex = 0;

    string boardString(board);
    if(index>80)
    {
        ofstream o("SudokuOutput.txt");
        for(j=0;j<81;j+=9){o<<(boardString.substr(j,9))<<endl;}
        return true;
    }

    if(board[index]!=' ')
    {
        return solve(board, index+1);
    }

    column = index % 9;
    row = index/9 * 9;
    startIndex = index / 9 / 3 * 27 + column / 3 * 3;

    string notAllowed;

    for(j = column; j<81; j+=9)
    {
        notAllowed += board[j];
    }

    notAllowed += boardString.substr(row, 9);

    for(j = startIndex; j<startIndex+27; j+=9)
    {
        notAllowed+= boardString.substr(j,3);
    }

    for(j = 1; j<10; j++)
    {
        stringstream n;
        n<<(j);
        if(notAllowed.find_first_of(n.str())==string::npos)
        {
            char copyBoard[81];
            copy(board,board+81,copyBoard);
            copyBoard[index] = (unsigned char)j+48;
            if(solve(copyBoard, index + 1))
            {
                return true;
            }
        }
    }
    return false;
}

int main()
{
    int j = 0;
    char board[81];
    fstream f("Sudoku.txt");

    while(f>>noskipws>>board[j])
    {
        if(board[j]!='\n')
        {
            j++;
        }
    }
    solve(board);

}
