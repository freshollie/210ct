#include <iostream>
#include <fstream>
#include <sstream>

int size;

using namespace std;

ofstream write_file;

int ball_roll(int x, int y, string output, int *matrix)
{
    int pos_list[4][2]= {
                            {-1,  -2},
                            {-2, -1},
                            {-1, 0},
                            {0, -1}
                            };

    bool notFound = true;

    for (int i = 0; i<4 ; i++)
    {
        int x2 = pos_list[i][0] + x;
        int y2 = pos_list[i][1] + y;

        if (0<= x2 && x2< size && 0 <= y2 && y2 < size) {
            if (matrix[y2 * size + x2] < matrix[(y-1) * size + x-1]) {
                cout<<matrix[y2 * size + x2]<<matrix[(y-1) * size + x-1];

                stringstream ss;
                ss<< output << y << " " << x << endl;

                ball_roll(x2+1, y2+1, ss.str(), matrix);
            }
        } else {
            if (notFound){
                write_file << output << y << " " << x << endl << endl;
            }
            notFound = false;
        }

    }


}

int main()
{
    fstream f("play.txt");
    int i,j;
    int p[2];

    f>>size>>p[0]>>p[1];
    int m[size][size];

    for(i=0;i<size;i++)
    {
        for(j=0;j<size;j++)
        {
            f>>m[j][i];
        }
    }

    write_file.open("playOutput.txt");
    ball_roll(p[1], p[0], "", &m[0][0]);
}
