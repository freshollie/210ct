'''
3.Given two strings of n and m integer elements, write the pseudocode to compute:
a) The string that contains all the elements belonging to both strings.
b) The string of all the elements of the two given strings, written once.
c) The string of the elements from the first string, without the elements that are also in the
second string.
What's the run time?

function in_both_strings(n, m) do // Strings have to be integers
    alreadyIn <- array(10) // empty array of length 10 used to store if the
                           // the number has been output
    outputString <- ''

    for i <- 1 to Length(n) do
        for j <- 1 to Length(m) do
            // If element in the second string
            // and the number is not already in the output list

            if n[i] = n[j] and alreadyIn[int(n[i])+1] != True then
                outputString <- outputString + n[i] // Add the number to the output string
                alreadyIn[int(n[i])+1] <- True // Set that the number is in the output string

    return outputString

function all_elements(n, m) do
    alreadyIn <- array(10) // empty array of length 10 used to store if the
                           // the number has been output
    outputString <- ''
    for i <- 1 to Length(m) do
        alreadyIn[int(m[i])+1] <- True // Set that the element has been used

    for i <- 1 to Length(n) do
        alreadyIn[int(n[i])+1] <- True

    for i <- 1 to Length(alreadyIn) do # Go through each used number
        if alreadyIn[i] = True then
            outputString <- outputString + string(i-1) // Add used number to output

    return outputString

function no_second(n, m) do
    alreadyIn <- array(10)// empty array of length 10 used to store if the
                          // the number has been output
    outputString <- ''

    for i <- 1 to Length(n) do
        alreadyIn[int(n[i]+1] <- True

    for i <- 1 to Length(m) do
        if alreadyIn[int(m[i])+1] != True then
            outputString <- outputString + string(m[i])

    return outputString

in_all_strings: O(n^2) efficiency
all_elements: O(3n) efficiency
no_second: O(2n) efficiency
'''
