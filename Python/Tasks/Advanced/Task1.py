'''
1. Write a program to predict the number of creatures in a fictional alien invasion. An alien
lays X eggs each day (there are no genders in this species) and the eggs hatch after Y days. If
X is 3 and Y is 5, how many aliens will there be 30 days after a single alien invades?
'''
hatchDays = 5
eggsPerDay = 3
aliens = 1

eggs = [0] * hatchDays # Queue of number of days for an egg to hatch

for i in range(30): # Repeat for number of days

    aliens += eggs[-1] # Hatch the number of mature eggs

    eggs.pop() # Take those eggs off the front of the maturing queue
    eggs.insert(0, aliens * eggsPerDay) # Add the new laid eggs to the back of the maturing

print(aliens)
print('\n')
