'''
13. Using the graph structure previously implemented, implement a function
isConnected(G) to check whether or not the graph is strongly connected. The
output should be simply 'yes' or 'no'.
'''

from Task12 import Graph

class NewGraph(Graph):
    '''
    Graph object.

    Each node contains the index values
    of the nodes it links to. Wont work for
    deleteing nodes.

    '''

    def isConnected(self):
        for startNode in range(len(self.nodes)): # Iterate over all of the nodes

            for endNode in range(len(self.nodes)):
                # Check if there is a path between the 2 nodes and that they are not the same node

                if startNode != endNode and not self.isPath(startNode, endNode):

                    return False
        return True

if __name__ == '__main__':
    g = NewGraph()
    g.addNode([])
    g.addNode([])
    g.addNode([1,0])
    n = g.addNode([g.addNode([1])])
    g.addNode([n])

    print('Yes' if g.isConnected() else 'No')
