'''
8. Adapt the quick sort algorithm to find the mth smallest element out of a sequence of
n integers.
'''

'''
algorithm quicksort(A, lo, hi) is
    if lo < hi then
        p := partition(A, lo, hi)
        quicksort(A, lo, p – 1)
        quicksort(A, p + 1, hi)

algorithm partition(A, lo, hi) is
    pivot := A[hi]
    i := lo     // place for swapping
    for j := lo to hi – 1 do
        if A[j] ≤ pivot then
            swap A[i] with A[j]
            i := i + 1
    swap A[i] with A[hi]
    return i
'''


'''
Algorithm adapted from the wikipedia psuedocode
'''
def partition(sequence, lo, hi):
    pivot = sequence[hi]
    i = lo

    for j in range(lo, hi):
        if sequence[j] <= pivot:
            sequence[i], sequence[j] = sequence[j], sequence[i]
            i+=1

    sequence[i], sequence[hi] = sequence[hi], sequence[i]
    return i

def quicksort(sequence, lo, hi):
    if lo < hi:
        p = partition(sequence, lo, hi)
        quicksort(sequence, lo, p - 1)
        quicksort(sequence, p + 1, hi)

sequence = [0, 7, 2, 3, 1]
m = 3 - 1 # mth smallest value
quicksort(sequence, 0, 4)
print(sequence[m])
