'''
12. Implement the structure for an unweighted, undirected graph G, where nodes
consist of positive integers. Also, implement a function isPath(v, w), where v and w
are vertices in the graph, to check if there is a path between the two nodes. The
path found will be printed to a text file as a sequence of integer numbers (the node
values).
'''

class Graph:
    '''
    Graph object.

    Each node contains the index values
    of the nodes it links to. Wont work for
    deleteing nodes.

    '''
    def __init__(self):
        self.nodes = []

    def addNode(self, linksTo=None):
        '''
        Adds a node by appending the links to the nodes
        list. It then checks all the nodes to make sure that
        older nodes now contain updated values

        :param linksTo:
        :return: Added Node position
        '''
        if not linksTo:
            linksTo = []
        self.nodes.append(linksTo)
        self.checkLinks()
        return len(self.nodes)-1 # Return the position of the node added

    def checkLinks(self):
        for node in range(len(self.nodes)): # Iterate through each node

            for link in self.nodes[node]: # Iterate for each link that that node

                if node not in self.nodes[link]: # If the node isn't in the links of the node it links to
                    self.nodes[link].append(node) # Add the link back from the other node

    def isLinked(self, node1, node2):
        return self.nodes[node1] in self.nodes[node2]

    def path(self, start, end, currentPath=None):
        '''
        Attemptes to recursively find a path on the graph
        between the given nodes.

        :param start:
        :param end:
        :param currentPath:
        :return:
        '''
        if not currentPath:
            currentPath = []

        for node in self.nodes[start]: # Iterate over every link from the node
            if node in currentPath: # If already been to this node, don't even try it
                continue

            if node == end: # If the node is the end
                currentPath.append(end) # Add the end node to the path
                return currentPath # Return the path
            else:
                found = self.path(node, end, currentPath+[start]) # Otherwise, recur with that node
                if found: # If the result of the recursion is not false
                    return found # A path has been found so return it

        return False # No path was found from this node, so return false to show this

    def isPath(self, start, end):
        if self.path(start, end, currentPath=[start]):
            # If anything other than false was returned from this then
            # its a valid path
            return True
        else:
            return False

if __name__ == '__main__':
    g = Graph()
    g.addNode() # Make a node
    g.addNode([0]) # Make another node with link to the first node
    g.addNode([1,0]) # Make another node with a link to both node 1 and node 2

    n = g.addNode([g.addNode([2])]) # Make another node with a link to another node which has a link to node 2
    # And keep the resulted node ID

    '''
    0   1
     \ /
      2
       \
        3
         \
          4
    '''

    if g.isPath(0, n):
        with open('path.txt', 'w') as f: f.write(str(g.path(0, n))) # Write that path to the file
