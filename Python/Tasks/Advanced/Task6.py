'''
6. Consider having n cubes, each being characterized by their edge length and their
colour. Use the cubes (not necessarily all of them) in order to build a tower of
maximum height, under the following conditions:
 a) any two neighbouring cubes must be of different colours.
 b) the edge length of a cube is lower than the edge length of the cube placed
below it.
'''

'''
function buildTower(cubes):
    unsortedCubes <- cubes
    sortedCubes <- new Array
    sortedCubes append unsortedCubes[1]
    remove unsortedCubes[1]

    for i <- 1 to length(unsortedCubes) o
        for j <- 1 to length(sortedCubes) do
            if sortedCubes[j] size > ubsortedCubes[i]:
                insert unsortedCubes[i] at sortedCubes[j]

    cubes <- sortedCubes

    removeCubes <- []

    for cubeIndex <- 1 to length(cubes-1):
        cube <- cubes[cubeIndex]

        if cube colour = cubes[cubeIndex + 1] colour or \
           cube size = cubes[cubeIndex + 1] size do
            removeCubes append cube

    for cube in removeCubes:
        cubes remove cube

    return cubes
    
'''

import random

class Cube:
    '''
    Cube object used to hold colour
    and edge size values
    '''
    def __init__(self, edgeSize, colour):
        self._edgeSize = edgeSize
        self._colour = colour

    def getEdge(self):
        return self._edgeSize

    def getColour(self):
        return self._colour

    def __repr__(self):
        '''
        Represents the cube as its edgesize
        and colour
        '''
        return str(self._edgeSize) + ' ' + str(self._colour)

cubes = [Cube(random.randint(1,15), random.randint(0,3)) for i in range(20)]
# Make a list of 20 random cubes

def buildTower(cubes):
    cubes.sort(key = lambda cube: cube.getEdge(), reverse = True)
    # Sort the cubes by edge size

    for cubeIndex in range(1, len(cubes))[::-1]: # Go through the cube list backwards
        cube = cubes[cubeIndex]

        if cube.getColour() == cubes[cubeIndex - 1].getColour() or \
           cube.getEdge() == cubes[cubeIndex -1].getEdge():
            # Check if the cubes next to each other are the same
            # Colour or have the same edge length and if they are remove the cube from the tower

            del cubes[cubeIndex]
    return cubes

for cube in buildTower(cubes): print(cube)
