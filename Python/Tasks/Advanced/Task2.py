'''
2. A sparse matrix is a matrix where the number of elements which are zero is bigger than the
number of elements which are not zero. Find a way to store sparse matrices, and write the
functions to add, subtract, and multiply pairs of such matrices. Do not use predefined
functions for the operations on matrices in your programming language of choice.
'''

def createMatrix(x, y):
    '''
    Creates a matrix with dimensions x by y
    '''
    return [[0] * x for i in range(y)]

def multiply(matrix1, matrix2):
    '''
    Performs a standard matrix multiplication function
    on the 2 given matrices and returns the new matrix
    '''

    outputMatrix = createMatrix(len(matrix2[0]), len(matrix1)) # Make the new output matrix of the correct dimensions

    for i in range(len(matrix2[0])):
        # Iterate over the width of the second matrix

        for y in range(len(matrix1)):
            # Iterate over the height of the first matrix

            for x in range(len(matrix1[0])):
                # Iterate over the width of the first matrix

                # Translation:

                # outputMatrix cell row = height iteration,  column = width iteration of the second matrix
                # + (matrix1 cell row = height iteration, column = width iteration of the first matrix

                # * matrix2 cell row = width iteration of the first matrix,
                # height = width iteration of the second matrix

                outputMatrix[y][i] += matrix1[y][x] * matrix2[x][i]

    return outputMatrix

def subtract(matrix1, matrix2):
    output = createMatrix(len(matrix1[0]), len(matrix1))
    for y in range(len(matrix1)):
        for x in range(len(matrix1[0])):
            output[y][x] = matrix1[y][x]-matrix2[y][x]

    return output

def add(matrix1, matrix2):
    output = createMatrix(len(matrix1[0]), len(matrix1))
    for y in range(len(matrix1)):
        for x in range(len(matrix1[0])):
            output[y][x] = matrix1[y][x]+matrix2[y][x]

    return output


if __name__ == '__main__':
    matrix1 = [
        [7, 3],
        [2, 5],
        [6, 8],
        [9, 0]
    ]

    matrix2 = [
        [7, 4, 9],
        [8, 1, 5]
    ]

    matrix3 = [
        [1,2,3],
        [1,2,3],
        [6,4,2]]

    matrix4 = [
        [1, 8, 3],
        [1, 2, 3],
        [6, 4, 2]]


    for row in multiply(matrix1, matrix2):
        print(row)

    for row in add(matrix3, matrix4):
        print(row)

    for row in subtract(matrix3, matrix4):
        print(row)
