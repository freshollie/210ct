'''
7. Let's consider a labyrinth as a n × m matrix, where the corridors are denoted by 1s
situated in consecutive positions on the same line or column. The rest of the
elements are 0. Within the labyrinth, a person is considered to be in position (i, j).
Write a program that lists all exit routes which do not pass the same place twice.
Input: n, m, the rows of the matrix, the coordinates of the exit and the coordinates of
the person (row, column). Output: a sequence of row/column pairs representing the
person's successive position.
'''

def path(matrix, position, currentPath = None):
    '''
    My algorithm works by checking all the directions not including the direction it came from.
    It checks the new position against where it has been and stops the path
    if it ends up in a loop.

    position: the current matrix position
    matrix: the matrix
    currentPath: where it has been
    '''

    if currentPath == None:
        currentPath = []

    foundPath = False # Used to indicate that this path was or wasn't a success
    for nextPoint in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
        nextPos = position[:]
        nextPos[1] += nextPoint[0]
        nextPos[0] += nextPoint[1]

        if nextPos[1] in [len(matrix[0]), -1] or \
            nextPos[0] in [len(matrix), -1]: # Outside of the laberyinth

            yield currentPath

        else:
            if (nextPos not in currentPath and
                  matrix[nextPos[0]][nextPos[1]] != 1): # If the next pos is not a wall

                returnedPath = path(matrix, nextPos, currentPath[:] + [nextPos]) # Find all possible paths for the next position
                if returnedPath: # If anything other than false returned then a path has been found
                    foundPath = True
                    for p in returnedPath: # Yield all found paths
                        yield p
    if not foundPath: # No path was found, so tell the previous function
        return False

matrix = [
    [1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 0],
    [1, 0, 0, 0, 0, 0, 1],
    [1, 1, 0, 1, 1, 0, 1],
    [0, 1, 0, 1, 1, 0, 1]
]

position = [3, 2]
    #break

# Map all the paths to show it works
for o in path(matrix, position, currentPath = [position]):
    print(o)
    for k in o:
        matrix[k[0]][k[1]] = 8

print('\n')

for row in matrix:
    print(row)