'''
4. Write the pseudocode for a recursive program to generate the Cartesian product
(product set, direct product, cross product) of n sets.

(2,1,6) * (3, 4, 5, 7) = [(2,3),(2,4),(2,5),(2,7)...]

* (9, 10, 11) = [(9, 2, 3), (9, 2, 4), ....]

[(2,3,2,2,1,3,4...),(2,234,32,36,46,24,56,3,35...),....]

function product(sets):
    if length(sets) = 2 then
        result2 <- sets[2]
    else:
        result2 <- product(sets[2:])

    result <- array(length(sets[1]) * len(result2))

    x <- 1

    for i <- 1 to length(sets[1]) do
        for j <- 1 to length(result2) do
            if type(result2[j]) = int then
                result[x] ><- (sets[1][i], result2[j]))
            else
                result[x] <- (sets[1][i],) + results2[j])

            x <- x + 1

    return result
'''

def product(sets):
    '''
    returns the product of the given sets
    '''

    if len(sets) == 2:
        # If the number of sets is 2
        # then the result is the first set times
        # the second
        result2 = sets[1]
    else:
        # Other wise the second set needs
        # to be found out of the product
        # of the remaining sets

        result2 = product(sets[1:])

    result = [0] * len(sets[0]) * len(result2)
    # the number of output sets is set to
    # the length of the first set times the length of
    # the second


    x = 0

    for i in range(len(sets[0])):
        for j in range(len(result2)):

            if type(result2[j]) == int: # If the set is just 1
                result[x] = (sets[0][i], result2[j])
            else:
                # Add to the current sets
                result[x] = ((sets[0][i],) + result2[j])

            x += 1 # Move to the next set in the result

    return result

print(product([(5,4), (1,2), (3,5)]))