'''
14. Consider the structure of a directed weighed graph. Implement an algorithm to find
its maximum cost spanning tree. The output should be the preorder and postorder
traversal of the tree. Describe the running time of this algorithm.
'''

from Task13 import NewGraph

class WeightedGraph(NewGraph):
    '''
    WeightedGraph stores dictionaries as the node.

    The dictionary contains the node it links to and the
    weight of the connection.

    EG:
    [
        {
            1: 6,
            2: 1,
            3: 5
        },

        {
            0: 6,
            2: 4
        },

        {
            0: 1,
            1: 4
        },

        {
            0: 5
        }
     ]

    '''

    def addLink(self, fromNode, toNode, weight=1):
        self.nodes[fromNode][toNode] = weight # Adds the link the correct way around
        self.nodes[toNode][fromNode] = weight # Adds the link the other way around

    def addNode(self, linksTo = None):
        if not linksTo:
            linksTo = {}

        self.nodes.append(linksTo) # Add the link
        self.checkLinks() # Check to see if there are any links that don't contain the
        # links added

        return len(self.nodes)-1

    def checkLinks(self):
        for node in range(len(self.nodes)):  # Iterate through each node

            for link in self.nodes[node]:  # Iterate for each link that that node

                if node not in self.nodes[link]:  # If the node isn't in the links of the node it links to
                    self.nodes[link][node] = self.nodes[node][link] # Set the same weight for that link in the other node value

    def getEdgeWeight(self, fromNode, toNode):
        '''
        returns the weight of the specified edge
        '''
        return self.nodes[fromNode][toNode]

    def getSpaningTree(self, max=True):
        '''
        return a dictionary containing the node ids
        and the nodes that they link to to make the minimum
        or maximum spanning tree.

        eg {0:[1,2],1:[],2[3],3[5]}

        Algorithm works using a breath first search
        '''
        tree = {0:[]}
        largestValues = []
        child = 0

        if not self.isConnected():
            return False

        while len(list(tree.keys())) != len(self.nodes): # While it hasn't found all the nodes

            while True:
                if largestValues:
                    child, parent = largestValues.pop(0) # Take the largest or smallest value off the front of the list
                    if child not in tree: # Not used that node before
                        tree[parent].append(child) # Add it to the tree under the current parent
                        tree[child] = [] # And make its own parent node in our tree
                        break
                else:
                    break

            largestValues += list(zip(self.nodes[child].keys(), [child] * len(list(self.nodes[child].keys()))))
            # Map the key values to the node they came from

            largestValues.sort(key=lambda x: self.getEdgeWeight(x[1], x[0]), reverse=max)
            # Sort the values found by their edge weight from the node they connect to
            # If the list is reversed then the maximum spanning tree is found

        return tree # When all nodes found, return the tree found


    def printDescription(self):
        for node in range(len(self.nodes)):
            print('-'*10)
            print('Node %s links to nodes:' %(node))
            for link in self.nodes[node]:
                print('%s: with a weight of %s' %(link, self.nodes[node][link]))

    def printPreOrder(self, tree, parent=0):
        print(parent)
        for child in tree[parent]:
            self.printPreOrder(tree, child)

    def printPostOrder(self, tree, parent=0):
        for child in tree[parent]:
            self.printPostOrder(tree, child)
        print(parent)



w = WeightedGraph()

for i in range(4):
    w.addNode()

w.addLink(0, 1, 3)
w.addLink(1, 2, 4)
w.addLink(0, 2, 1)
w.addLink(0, 3, 5)
w.addLink(1, 0, 6)


print(w.nodes)
w.printDescription()
print(w.getSpaningTree())
w.printPostOrder(w.getSpaningTree())