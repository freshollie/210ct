'''
10. Using the model of a circular single-linked list, implement the following scenario:
210CT Coursework 2016/2017 4 of 6 N children stand in a circle;
 one of the children starts counting the others clockwise.
Every Nth child leaves the game. The winner is the one who remains.
Notes: Read the number of children, the childrens' names and the one starting to
count from the standard input. Input: 4; Diana, Michael, David, Mary; Start: Diana;
Winner: Michael.
'''

class Node(object):
    '''
    Node Object used to store the position of the
    next node and hold the value of the node
    '''
    def __init__(self, value):
        self.nextNode = None
        self.value = value

    def getValue(self):
        return self.value

    def setNextNode(self, nextNode):
        '''
        Set the link to the next node
        '''
        self.nextNode = nextNode

    def getNextNode(self):
        '''
        Returns the next node object
        '''
        return self.nextNode

    def __repr__(self):
        '''
        Used to represent the node when printing. Prints this nodes
        value with the next nodes value
        '''
        return 'Node(Value: %s NextNode: %s)' %(self.value, self.getNextNode().getValue())

class LinkedList(object):
    '''
    Linked list object is a circlular list and the next value is given when asked.
    Values can be dynamically removed and added.
    '''
    def __init__(self, values):
        self.currentNode = None
        self.links = []
        for value in values:
            self.addValue(value)

    def addValue(self, value):
        '''
        Add a value to the circular list
        '''
        self.links.append(Node(value)) # Make a new node

        self.links[-1].setNextNode(self.links[0])
        # Set the link to the last start

        if len(self.links)>1:
            # Set the previous link to link to the new link
            self.links[-2].setNextNode(self.links[-1])

    def removeNext(self):

        if not self.currentNode:
            self.currentNode = self.links[0]

        oldLink = self.currentNode.getNextNode()
        # Get the next node

        self.currentNode.setNextNode(oldLink.getNextNode())
        # set the new link of the node that isn't being deleted
        # to the node after the one that is being deleted

        self.links.remove(oldLink) # Remove the old node from the node

    def getNextValue(self):
        return self.currentNode.getNextNode().getValue()

    def moveNextValue(self):
        if not self.currentNode:
            self.currentNode = self.links[0]

        self.currentNode = self.currentNode.getNextNode()
        # move to the next node

        return self.currentNode.getValue() # Return that nodes value

    def getCurrent(self):
        return self.currentNode[0]

    def getLength(self):
        return len(self.links)

    def getValues(self):
        values = []

        for node in self.links:
            values.append(node.getValue())
        return values

    def __repr__(self):
        return str(self.getValues())

def circleGame(names, count):
    circle = LinkedList(names)

    while circle.getLength() > 1: # While people are still in a circle
        for i in range(count-1): # Only move forward n-1 as we remove the next value
            circle.moveNextValue() # Step to the next person

        circle.removeNext() # Remove the nth person

        circle.moveNextValue() # Correct position for next iteration

    return circle.getValues()[0] # Return last person left

print(circleGame(['Diana','Michael','David','Mary'], 4))
