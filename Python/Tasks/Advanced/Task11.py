
paragraph = '''
11. Build a Binary Search Tree (BST) to hold English language words in its nodes. Use
a paragraph of any text in order to extract words and to determine their frequencies.
Input: You should read the words and frequencies from a file in a suitable format,
such as .csv. The following tree operations should be implemented: a) Listing (word,
frequency) pairs for each of the tree nodes. b) Printing the tree in preorder. C)
Finding a word. Regardless whether found or not found your program should output
the path traversed in determining the answer, followed by yes if found or no,
respectively.
'''

def dummyOut(x):
    '''
    Used so we can keep our debug statements
    without needing to use if statments
    '''
    pass

class Tree:
    '''
    Tree object holds methods to store data in the
    internal tree object.

    Tree object is structure as follows:

    [Key, Value,
        Branches[

            [Key, Value,// A branch
                Branches[
                    [] // No branches
                ]
             ],

            [Key, Value,
                Branches[
                    [],

                    [Key, Value,
                        Branches[

                            [Key, Value,
                                Branches[
                                    []
                                ]
                             ]

                        ]
                    ]

                ]
            ]

        ]
    ]

    '''
    def __init__(self, pairs=[]):
        self.tree = []
        for pair in pairs:
            self.setValue(pair)

    def searchNode(self, key, node, deep = 1, output = dummyOut):
        '''
        Recursively searches for the place of a key in the tree.

        If the key doesn't exist, the position the key should be in is
        returned.

        :param key: Key to search for
        :param node: current node
        :param deep: How deep we are in the tree
        :param output: the fuction that takes the debug output
        :return:
        '''

        output('Level: %s Key: %s' %(deep, key))

        if not node: # If the node is empty, then this is where the name should be
            output('Found place for key')
            return node

        if node[0] == key: # If we found the key then this is the correct node
            output('Found key node')
            return node

        if key > node[0]: # If the key is larger than the current nodes key
            output('Node not key, %s is larger than %s. Branching right' %(key, node[0]))
            # We now recur and set the node to the right branch node
            # and return the output.
            return self.searchNode(key, node[2][1], deep + 1, output)
        else:
            # We now recur and set the node to the left branch node
            # and return the output.
            output('Node not key, %s is less than %s. Branching left' %(key, node[0]))
            return self.searchNode(key, node[2][0], deep + 1, output)

    def setValue(self, key, value, output = dummyOut):
        '''
        Sets a value for a specific key.

        If the key doesn't exist it makes the key

        :param key: Key to search
        :param value: Value to associate
        :param output: the debug output function
        :return:
        '''
        output('------Setting value for %s-------' % (key))

        # Get where the node should be in the tree
        # Or the place where the key is in the tree
        node = self.searchNode(key, self.tree, output= output)

        if node:
            # If the key exists, set its value
            node[1] = value
        else:
            # Otherwise make the key and set its value,
            # in the correct place
            node.append(key)
            node.append(value)
            node.append([[], []])

    def getValue(self, key, output = dummyOut):
        '''
        Tries to retrieve a value for a key,

        If it doesn't exist it will return 0

        :param key:
        :param output:
        :return:
        '''

        output('------Getting Value for %s-------' %(key))
        node = self.searchNode(key, self.tree, output = output)

        if node:
            return node[1]
        else:
            return 0

    def getTraverseInorder(self, startNode):
        '''
        Performs a recursive inorder traversal of the tree,
        and returns the output.

        This produces all the keys in alphabetical order

        :param startNode:
        :return:
        '''
        returnValues = []

        i = 0
        done = False
        for node in startNode[2]: # Loop for both left and right values.
            if node:
                returnValues += self.getTraverseInorder(node)
            if not i:
                done = True
                returnValues.append([startNode[0], startNode[1]])
            i += 1

        if not done:
            if startNode!=[]:
                returnValues.insert(0, [startNode[0], startNode[1]])

        return returnValues

    def getAllOrdered(self):
        return self.getTraverseInorder(self.tree)

    def getTraversePreOrder(self, startNode = None, returnValues =[]):
        '''
        Performs a recursive preorder traversal of the tree,
        and returns the output.

        This produces all the keys in alphabetical order

        :param startNode:
        :return:
        '''

        if startNode == None:
            startNode = self.tree # set the start node to the root node

        if startNode != []:
            returnValues.append([startNode[0], startNode[1]]) # Add the current node to the output
            for node in startNode[2]: #
                if node:
                    self.getTraversePreOrder(node, returnValues) # Do for each branch

        return returnValues

    def listAll(self):
        for key,value in self.getTraversePreOrder():
            print("%s: %s"%(key, value))

    def listInOrder(self):
        for key,value in self.getAllOrdered():
            print("%s: %s"%(key, value))


t = Tree()
with open('MyWords.csv') as wordFile: # Open the file

    for line in wordFile.readlines(): # Read each line in the file
        key, value = line.strip().replace(' ', '').split(',') # Ignore spaces and split the value and key
        t.setValue(key, int(value), print) # Insert into the tree
print(t.tree)
t.listAll() # List the pre order traversal

