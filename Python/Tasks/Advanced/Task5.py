'''
5. In addition to the normal homework task, write a function that takes four parameters
representing the constant and multiplier of two linearly growing (as in O(m × n + k) )
functions and determines the critical value of n (which should be an integer) at
which the relative run-time of the two algorithms switches. That is, at which input
size is algorithm A slower than B and at which is B slower than A? Use an iterative
approach rather than solving the equations.
'''

def time(m1, k1, m2, k2):
    n = 0
    lastResult = None
    while True:
        o1 = m1 * n + k1
        o2 = m2 * n + k2

        # Both o algorithms calculated

        if o1 > o2:
            if lastResult == False: # If result is different from the last result
                return n # return what data value made the switch
            lastResult = True
        elif o1 < o2:
            if lastResult:
                return n
            lastResult = False

        n += 1

print(time(3,1,2,5))
