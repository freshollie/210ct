'''
9. Write a function to calculate the kth power of a square matrix, using pointers to
access to the elements of the matrix. The resulted matrix will be displayed in natural
form.
'''

# Use the previously written matrix multipliers
from Task2 import multiply

def powerMatrix(matrix, n):
    for i in range(n-1):
        matrix = multiply(matrix, matrix) # keep doing the last result * matrix
    return matrix

myMatrix = [[21, 2, 3],
            [9, 2, 4],
            [8, 4, 5]]

for row in powerMatrix(myMatrix, 2):
    print(row)
