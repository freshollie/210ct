# Oliver Bell
# Code Golf week 2
# Updated Submission

# Code takes 25 characters input and then a 'Year' input
# Eg:
# HHHHHHHIHHHHFFHHHHHHHHHHH
# 2

p=input
g=list(p())
r = range
for i in r(int(p())):
    for j in r(25):
        f=g[j]
        if f=='I':
            for o in [-5, -1, 1, 5]:
                k = j + o
                if -1 < k < 25 and g[k]=='H':
                    g[k]='i'
            g[j]='f'
        elif f=='F':
            g[j]='h'
    g=[p.upper()for p in g]
for x in r(5, 26, 5):
    print(g[x-5:x])