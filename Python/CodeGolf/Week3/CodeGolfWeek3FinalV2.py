# Oliver Bell
# Code Golf week 3
# 599 Characters by my count

import math as n
ms = [[]]
l = list
r = range
p = print
def o(s,x):
    for l in s:
        for c in l:
            p(x[c],end=' ')
        p()
def gs(s,m,i):
    c=m//abs(m)
    a=l(r(s,s+(i*c),c))
    if i>2:
        i-=1
        b=l(r(a[-1]+m,(a[-1]+m)+(m*i),m))
        m*=-1
        c=gs(b[-1]+m//abs(m),m,i)
        return [a]+[b]+c
    else:
        return [a]+[[a[1]+m,a[0]+m]]
def gd(s):
    t=[]
    for u in [[1,0,r(s-1,-1,-1)],[0,1,r(1,s)]]:
        for v in u[2]:
            t.append([c[u[0]]*s+c[u[1]]for c in zip(r(v, s),r(s))])
    return t
with open('matrix.txt') as a:
    for h in a.readlines():
        if not h.strip():
            ms.append([])
        else:
            for i in h.strip():
                if i != ' ':
                    ms[-1].append(i)
for m in ms:
    w=int(n.sqrt(len(m)))
    for q in [gs(0, w, w), gd(w), [[j*w+i for j in range(w)]for i in range(w)]]:
        o(q,m)
        p()