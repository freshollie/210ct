# Oliver Bell
# Code Golf week 3
# 599 Characters by my count

import math
ms = [[]]
l = list
r = range
p = print
def o(s,x):
    for l in s:
        for c in l:
            p(x[c],end=' ')
        p()
def gs(s,m,i):
    c=m//abs(m)
    a=l(r(s,s+(i*c),c))
    if i>2:
        i-=1
        b=l(r(a[-1]+m,(a[-1]+m)+(m*i),m))
        m*=-1
        c=gs(b[-1]+m//abs(m),m,i)
        return [a]+[b]+c
    else:
        return [a]+[[a[1]+m,a[0]+m]]
def gd(s):
    u=l(r(s-1,-1,-1))+l(r(s,s*s,s))
    t=[]
    for z in u[:]:
        t.append([z])
        i=z+s+1
        while i not in u and i<s*s:
            t[-1].append(i)
            u.append(i)
            i+=s+1
    return t
with open('matrix.txt') as a:
    for h in a.readlines():
        if not h.strip():
            ms.append([])
        else:
            for i in h.strip():
                if i != ' ':
                    ms[-1].append(i)
for m in ms:
    w=int(math.sqrt(len(m)))
    for q in [gs(0, w, w), gd(w), [[j*w+i for j in range(w)]for i in range(w)]]:
        o(q,m)
        p()