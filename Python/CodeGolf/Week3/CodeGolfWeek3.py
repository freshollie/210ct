'''
C H A L L E N G E
Square matrices (N*N, where can be between 1 and 10) of uppercase letters are read from a file named
matrix.txt. The matrices in the file are separated by one empty line. Write a program that reads these matrices
from the file and implements the following operations: printSpiral, printDiagonal and flipMatrix. All of these
operations are exemplified below
'''

import math


#### SPIRAL #####

# Attempt 1
# Long, used a 2d list

def spiral(matrix):
    allUsed = []
    i = 0
    j = 0
    ran = []
    constantList = [[1, 0], [0, 1], [-1, 0], [0, -1]]
    ran.append([])

    while True:
        ran[-1].append([i, j])
        allUsed.append([i, j])

        lastConstant = constantList[0]

        for o in range(2):
            failed = False
            i += constantList[0][0]
            j += constantList[0][1]

            if [i, j] in allUsed or not -1 < i < len(matrix[0]) or not -1 < j < len(matrix):
                i -= constantList[0][0]
                j -= constantList[0][1]

                constantList.append(constantList.pop(0))

                failed = True
            else:
                break

        if failed:
            break

        elif lastConstant != constantList[0] and len(ran[-1]) > 1: # Changed method so start new list
            ran.append([])

    for line in ran:
        print(line)

    for line in ran:
        for coord in line:
            print(matrix[coord[1]][coord[0]], end = '')
        print('\n', end = '')


    '''

    spiral patterns
    ran = [0, 1, 2,
           5,

    ran = [0, 1, 2, 3,
           7, 11, 15,
           14, 13, 12,
           8, 4,
           5, 6,
           10, 9]

    ran = [0, 1, 2, 3, 4,
           9, 14, 19, 24,
           23, 22, 21, 20,
           15, 10, 5, # Used these as reference to generate the algorithm
           6, 7, 8,
           13, 18,
           17, 16,
           11, 12
           ]

    Diagonal patterns




    '''

# Attempt 2, ungolfed
# Much more efficient

def generateSpiral(start, multiple, i):
    constant = multiple//abs(multiple)

    r1 = list(range(start, start + (i * constant), constant))

    if i > 2:
        i -= 1

        r2 = list(range(r1[-1] + multiple, (r1[-1] + multiple) + (multiple * i), multiple))

        multiple *= -1

        r3 = generateSpiral(r2[-1] + multiple//abs(multiple), multiple, i)

        return [r1]+[r2]+r3

    else:
        return [r1]+[[r1[1] + multiple, r1[0] + multiple]]

def generateSpiralV2(multiple, matrix):
    i = multiple
    start = 0
    while 1:
        constant = multiple//abs(multiple)
        r1=list(range(start, start + (i * constant), constant))
        outputLine(r1, matrix)

        if i > 2:
            i -= 1
            r2 = list(range(r1[-1] + multiple, (r1[-1] + multiple) + (multiple * i), multiple))
            outputLine(r2, matrix)
            multiple *= -1
            start = r2[-1] + multiple//abs(multiple)
        else:
            outputLine([r1[1] + multiple, r1[0] + multiple], matrix)
            break

def generateSpiralV3(multiple):
    i = multiple
    start = 0

    while 1:
        j = start
        check = start + (i * constant)
        constant = multiple // abs(multiple)
        line = []
        step = constant

        while abs(j) < abs(check):
            line += [j]
            j += constant

        if i>2:
            i -= 1
            j = r1[-1] + multiple
            check = (r1[-1] + multiple) + (multiple * i)
            step = multiple
            multiple *= -1

        else:
            j = r1[1]

            outputLine([r1[1] + multiple, r1[0] + multiple], matrix)


    i = multiple
    start = 0
    while 1:
        constant = multiple // abs(multiple)
        r1 = list(range(start, start + (i * constant), constant))
        outputLine(r1, matrix)

        if i > 2:
            i -= 1
            r2 = list(range(r1[-1] + multiple, (r1[-1] + multiple) + (multiple * i), multiple))
            outputLine(r2, matrix)
            multiple *= -1
            start = r2[-1] + multiple // abs(multiple)
        else:
            outputLine([r1[1] + multiple, r1[0] + multiple], matrix)
            break

def output(sequence, matrix):
    for line in sequence:
        for coord in line:
            print(matrix[coord].strip(), end=' ')
        print('\n', end='')

def outputLine(items, matrix):
    print(' '.join(map(matrix.__getitem__, items)))


def printSpiral(matrix, width):
    output(generateSpiral(0, width, width), matrix)


####Diagonal#####
# Ungolfed, pretty efficient
'''
[[[3,0]],
 [[2,0], [3,1]]
'''

def generateDiagonal(size):
    used = list(range(size-1, -1, -1)) + list(range(size, size * size, size))
    result = []
    for z in used[:]:
        result.append([z])
        index = z + size + 1

        while index not in used and index < size * size:
            result[-1].append(index)
            used.append(index)
            index += size + 1

    return result

def newGenerateDiagonal(size, matrix):
    sequence = []

    for loopVariables in [[1, 0, range(size - 1, -1, -1)], [0, 1, range(1, size)]]:
        for value in loopVariables[2]:
                outputLine([coord[loopVariables[0]] * size + coord[loopVariables[1]]
                            for coord in zip(range(value, size), range(size))],
                           matrix)

    return sequence

def newGenerateDiagonalv2(start, stop, step, coord1, coord2):
    sequence = []
    while start<stop:
        outputLine([coord[coord1] * size + coord[coord2]
                    for coord in zip(range(value, size), range(size))],
                   matrix)
        start += step


    for loopVariables in [[1, 0, range(size - 1, -1, -1)], [0, 1, range(1, size)]]:
        for value in loopVariables[2]:
                outputLine([coord[loopVariables[0]] * size + coord[loopVariables[1]]
                            for coord in zip(range(value, size), range(size))],
                           matrix)

    return sequence


def printDiagonal(matrix, width):
    output(newGenerateDiagonal(width), matrix)


##### Flip matrix ######

def generateFlip(size):
    return [[j*size + i for j in range(size)] for i in range(size)]

def flipMatrix(matrix, width):
    output(generateFlip(width), matrix)

def newFlipMatrix(width, matrix):
    for i in range(width):outputLine([j * width + i for j in range(width)], matrix)

for matrix in open('matrix.txt').read().split('\n\n'):
    matrix = ' '.join(''.join(matrix).split('\n')).split()
    matrixWidth = int(math.sqrt(len(matrix)))
    print(matrixWidth)
    generateSpiralV2(matrixWidth, matrix)
    print('')

    newGenerateDiagonal(matrixWidth, matrix)
    print('')

    newFlipMatrix(matrixWidth, matrix)
    print('')

#for v in N:O(c[1] * W + c[0] for c in zip(, N))
