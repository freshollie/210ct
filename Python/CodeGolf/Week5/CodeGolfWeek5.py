def solve(board, index = 0):
    if index == 81:
        f = open('SudokuOut.txt',"w")
        for i in range(9): f.write(''.join(board[i*9:i*9+9])+'\n')
        return True

    if board[index] != ' ':
        return solve(board, index + 1)

    column = index % 9
    row = index // 9 * 9
    startIndex = index // 9 // 3 * 27 + column // 3 * 3

    notAllowed = board[column::9] + board[row:row+9] + board[startIndex: startIndex+3] + board[startIndex+9:startIndex + 12] + board[startIndex+18: startIndex+21]

    for number in range(1,10):
        if str(number) not in notAllowed:
            copyBoard = board[:]
            copyBoard[index] = str(number)
            if solve(copyBoard, index + 1):
                return True

solve(list(open('Sudoku.txt').read().replace('\n','')))
