import random as w
b=list(open('Sudoku.txt').read().replace('\n',''))
n=1
while n:
    n=0
    i=0
    e=b[:]
    while i<81:
        if e[i]==' ':
            c=i%9
            r=i//9*9
            u=i//9//3*27+c//3*3
            e[i]=str(w.randint(1,9))
            if e[i] in e[c::9]+e[r:r+9]+e[u:u+3]+e[u+9:u+12]+e[u+18:u+21]:
                i=82
                n=1
        i+=1
f = open('SudokuOut.txt', "w")
for i in range(9):f.write(''.join(e[i*9:i*9+9])+'\n')