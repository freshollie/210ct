'''
Puts random numbers into the algorithm to see if it works
May never complete
'''

import random as r
board = list(open('Sudoku.txt').read().replace('\n',''))
notDone = 1
while notDone:
    notDone = 0
    i=0
    copyBoard = board[:]
    while i<81:
        if copyBoard[i]==' ':
            column = i % 9
            row = i // 9 * 9
            startIndex = i // 9 // 3 * 27 + column // 3 * 3
            notAllowed = copyBoard[column::9] + copyBoard[row:row+9] + copyBoard[startIndex: startIndex+3] + copyBoard[startIndex+9:startIndex + 12] + copyBoard[startIndex+18: startIndex+21]
            copyBoard[i] = str(r.randint(1,9))
            if copyBoard[i] in notAllowed:
                i=82
                notDone = 1
        i+=1
f = open('SudokuOut.txt', "w")
for i in range(9): f.write(''.join(copyBoard[i * 9:i * 9 + 9]) + '\n')