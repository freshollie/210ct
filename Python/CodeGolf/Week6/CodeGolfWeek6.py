board=[' ']*9
message = 0
turn = 'X'
y = 'O'
while ' ' in board and message == 0:
    index = int(input(turn + "'s go: "))
    if board[index] == ' ':
        board[index] = turn

    for i in range(0,9,3):
        print(''.join(board[i:i+3]))
        if {turn} in map(set, [board[i//3::3], board[i:i + 3], board[8::-4], board[2:8:2]]):
            message=turn

    turn, y = y, turn
print(message)