'''
inputExample

5
2 3
1 3 2 4 3
0 7 6 2 1
7 5 4 4 3
7 2 2 2 4
0 1 0 5 8
'''
resultFile = open('playOutput.txt','w')
matrix = [line.split() for line in open('play.txt').readlines()]
size = int(matrix.pop(0)[0])
y, x = matrix.pop(0)

for line in matrix:
    print(line)

print(x, y)

def ballRoll(x, y, previous=[], output=''):
    notFound = True
    for x2, y2 in [(-1, -2), (-2, -1),(-1, 0),(0, -1)]:
        if (0 <= x2+x < size) and (0 <= y2+y < size):
            if matrix[y2+y][x2+x] < matrix[y-1][x-1]:
                ballRoll(x2+x+1, y2+y+1, previous + [(x, y)],output+'%s %s\n'%(y, x))
        else:
            if notFound: resultFile.write(output+'%s %s\n'%(y,x)+'\n')
            notFound = False

ballRoll(int(x),int(y))


R=open('playOutput.txt','w')
S,p,*M=[l.split()for l in open('play.txt').readlines()]
S=int(S[0])
def B(x,y,c=[],d=''):
    F=1
    for X, Y in [(-1, -2),(-2, -1),(-1, 0),(0, -1)]:
        if (0<=X+x<S)and(0<=Y+y<S):
            if M[Y+y][X+x] < M[y-1][x-1]:
                B(X+x+1,Y+y+1,c+[(x,y)],d+'%s %s\n'%(y,x))
        else:
            if F:R.write(d+'%s %s\n'%(y,x)+'\n')
            F=0
B(int(p[1]),int(p[0]))
