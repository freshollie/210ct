# Coursework from 210CT

210CT is the second year module on computer programming algorithms at coventry university.

This repository contains, the course work organised into the weeks it was set, and the code golf challenge for that week.

I completed some of the work in C++ and some in Python